@extends('master')


@section('content')



<div class="header-container" style="background-image: url(img/jpg/sub-hero-4.jpg); background-position: center bottom;">

    <div id="announcemnet" class="">

        <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location. <a href="{{ route('subscription') }}">Start subscription</a></p>

    </div>

    <header class="wrapper clearfix">

        @include('pages.partials.nav')

        <div id="heroContainer" class="clearfix heroFont animated fadeInDown" style="text-align: center;">
            <h1>Frequently asked questions</h1>
            <h2>Let us sort this out for you.</h2>

            <a href="/subscription" class="button">Start Subscription</a>
        </div>
    </header>
</div>


<div id="instructionsContainer" class="wrapper clearfix">
    <!-- INSTRUCTIONS BLOCK -->
    <article id="instructions">
        <div class="articleContent faq" style="margin: 0 auto; float: none; width: 70%;">
            <header>
                <h1>Frequently asked questions</h1>
                <p>You simply decide how many capsules you want in your mailbox each month and what flavour/strenght you fancy. Once you’re done with that we’ll need your address and credit card number, and we will solve the rest.</p>
            </header>
            <section>
                <h4>What type of machine do I need for these coffee capsules?</h4>
                <p>These capsules fits into the Nespresso machine only.</p>
            </section>

            <section>
                <h4>What type of subscription should I buy? How many capsules do I need every month?</h4>
                <p>Our most popular choice is the MIDI-package subscription, that contains 100 coffee capsules delivered to you every month. That means that you can drink about 3 coffees everyday plus some extra cups for guests or special occasions.
				</p>
            </section>
            <section>
                <h4>What if I feel that I chose the wrong amount of capsules? Is it possible to change the subscription?</h4>
                <p>Yes, your are free to change to a larger or smaller subscription package at any time. Just remember that the delivery only comes once a month and we cant change a subscription at the same month if the payment already went thru. </p>
            <section>
            </section>
                <h4>I’m going on a vacation for two months and don’t need any coffee at that time, do I need to end my subscription at House Coffee Club?</h4>
                <p>Our customers means a lot to us. With that being said, you are allowed to put your subscription on hold if you are going away for a longer time. Just remember that you have to tell us a month ahead.</p>
            <section>
            </section>
                <h4>I’m moving to another address but I want to keep my coffee subscription, is that possible?</h4>
                <p>Yes of course, just be sure to change your delivery address in your personal info on the website. </p>
            <section>
        </div>

    </article>
</div> <!-- #instructions-container -->

@stop
