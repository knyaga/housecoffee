<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>HouseCoffeeClub - Coffee capsule subscription service, Nespresso</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div id="priceTiers" style="min-height: 400px;">

    <div id="announcemnet" class="">

        <p><img src="img/flag/spain.gif" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>


    </div>

    <header class="wrapper clearfix" style="padding-top:46px;">
       @include('pages.partials.nav')
    </header>

    <div id="subHeroContainer" class="wrapper clearfix heroFont" style="margin-top:0;">
        <h1 class="animated fadeInRight">Thank you {{ $user->firstName }} <br /></h1>
        <h2 class="animated fadeInRight">Every end month we'll be sending your next batch of coffee capsules</h2>

        @if(Session::has('message'))
            <h2 class="animated fadeInRight">{{ Session::get('message') }}</h2>
        @endif

    </div>

</div>



<div id="orderFormContainer" class="wrapper clearfix">

    <div id="HccForm" class="animated fadeInLeft">

        <h1>Receipt  </h1>

        <div class="halfCol" style="width:50%; float: left; display:block;">
            <p>
                {{ $user->firstName }}, {{ $user->firstName }} <br />
                {{ $user->address1 }} <br />
                {{ $user->city }} <br />
                Order number <br />
                Time of purchase ( date and time )<br />
                {{ $user->email }}
            </p>
        </div>

        <div class="halfCol" style="width:50%; float: left; display:block;">
            <p>
                <em>House Coffee Club</em> by Copygram AB <br />
                Storgatan 31 <br />
                311 31 - Falkenberg<br />
                Phone: 0046(0)346-92302 <br />
                Organisationsnummer: 556866-6654 <br />
                VAT-number: SE5568666665401   <br />
            </p>
        </div>

        <div class="fullCol" style="width:100%; float: left; display:block;">
            <p>
                <h3>Order Information </h3>
                Name of item, amount, item cost   <br />
                Total amount   <br />
                VAT 25%     <br />
            </p>
        </div>

        <div class="fullCol" style="width:100%; float: left; display:block;">
            <h3>Support questions</h3>
            <p>Questions about your order or shipping details, please drop us a line at support@housecoffeeclub.com and we’ll get back to you as soon as possible.</p>
        </div>

    </div>

</div>







<div id="footerContainer" class="footer-container">
    <footer class="wrapper">
        <nav>
            <ul>
                <li><a href="#">Shipping info</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Press</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>

        <!-- <nav style="float: right;">
            <ul>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Twitter</a></li>
            </ul>
        </nav> -->
    </footer>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

<script src="js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>
</body>
</html>
