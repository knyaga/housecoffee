@extends('master')

@section('content')

        <div id="priceTiers" style="min-height: 300px;">

            <div id="announcemnet" class="">

            <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>

            </div>
            <!-- INCLUDES MENU LINKS -->
            @include('pages.partials.dashboard-nav')

        </div>

        <!-- INCLUDES SUB-ACCOUNT MENU -->
        @include('pages.partials.subAccountMenu')


        <div id="orderFormContainer" class="wrapper clearfix">

            <div id="HccForm" class="animated fadeInLeft">

            {!! Form::open(['action'=>'AccountsController@updateCard','id'=>'subscription-form']) !!}
                <!-- Header -->
                <h2 class="left">Payment Information</h2>

                @if(Session::has('card_message'))
                    <h2>{{ Session::get('card_message') }}</h2>
                @endif

                <h2>
                    @if( Auth::check() && $user->subscribed() )
                        @if( $user->cancelled() )
                            <p>Your subscription ends on {{ $user->subscription_ends_at->format('D d M Y') }}</p>
                        @endif
                    @endif
                </h2>

                <h2>
                 <div class="stripe-errors"></div>
                </h2>

                <!-- CREDIT CARD INFORMATION -->
                <input class="creditcard required" id="pay_creditcard" data-stripe="number" name="creditcard" placeholder="Creditcard number" type="creditcard">
                <input class="month required payThird" id="pay_month" data-stripe="exp-month" name="month" placeholder="month (dd)" type="text">
                <input class="year required payThird" id="pay_year" data-stripe="exp-year" name="year" placeholder="year (yy)" type="text">
                <input class="cvc required payThird" id="pay_cvc"  data-stripe="cvc"  name="cvc" placeholder="cvc/cvv" type="text" style="margin-right: 0;">


                <!-- ORDER TOTAL WILL BE SHOWN HERE -->
                <h2 class="left">Monthly Total:</h2>
                <h2 class="right" style="font-weight: normal">15€</h2>

                <!-- DISCLAIMER AND INFORMATION ABOUT THE SALES -->
                <p class="order-form-terms-disclaimer">
                    Payment is handled securly by <a href="http://stripe.com" target="_new">stripe.com</a>, you can use your standard credit card to pay.<br>
                    By placing an order your agreeing on our <a href="/terms">Sales Conditions, Terms</a> &amp; <a href="/retur">Return policy</a>
                </p>

                <!-- SEND FORM DATA TO PAYPAL FOR RECURRING PAYMENTS SETUP -->
                <input class="buttonPrimary" name="button" type="submit" value="Update card details" id="stripeCommandButton" />

            {!! Form::close() !!}

            @if( Auth::check() && $user->subscribed())
                @if( !$user->cancelled())
                    {!! Form::open(['route'=>'cancel-subscription']) !!}
                    <input class="buttonCancel" name="button" type="submit" value="Stop my Coffee Subscription" id="cancelButton" />
                    {!! Form::close() !!}
                @endif
            @endif

            @if( Auth::check() && $user->cancelled())
                {!! Form::open(['route'=>'resume-subscription']) !!}
                <input class="buttonCancel" name="button" type="submit" value="Resume my Coffee Subscription" id="cancelButton" />
                {!! Form::close() !!}
            @endif

            </div>

        </div>

@stop

@section('scripts')

    <!-- MAKE SURE JAVASCRIPT IS ENABLED -->
    <noscript><p>JavaScript is required for the registration form.</p></noscript>

    <script src="https://js.stripe.com/v2/"></script>
    <script src="/js/stripe.js"></script>

@stop
