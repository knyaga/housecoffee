@extends('master')

@section('content')

<div class="header-container">

    <div id="announcemnet" class="">

        <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location. <a href="{{ route('subscription') }}">Start subscription</a></p>

    </div>

    <header class="wrapper clearfix">

        @include('pages.partials.nav')

        <div id="heroContainer" class="clearfix heroFont animated fadeInDown">
            <h1>Coffee capsules on a montly subscription</h1>
            <h2>Tastefull, clean and comfortable delivery in your mailbox</h2>

            <a href="/subscription" class="button">Start subscription</a>
        </div>
    </header>
</div>

<!-- <div class="brands"></div>-->


<div id="priceTiers">
    <div id="subHeroContainer" class=" wrapper clearfix heroFont">
        <h1>Coffee capsules have never been easier</h1>
        <h2>Tastefull, clean and comfortable delivery in your mailbox</h2>
    </div>
    <div id="priceContainer" class="wrapper">
        <div class="fourcol">
            <h3><span>mini</span>15€</h3>
            <a href="new-order.html">order now</a>
            <ul>
                <li><strong>50 capsules/month</strong></li>
                <li>Fresh, tastefull coffee</li>
                <li>Mailbox delivery</li>
                <li>inc. tax & shipping </li>
            </ul>
        </div>
        <div class="fourcol popular">
            <h3><span>midi</span>29€</h3>
            <a href="new-order.html">order now</a>
            <ul>
                <li><strong>100 capsules/month</strong></li>
                <li>Fresh, tastefull coffee</li>
                <li>Mailbox delivery</li>
                <li>inc. tax & shipping </li>
            </ul>
        </div>
        <div class="fourcol">
            <h3><span>maxi</span>59€</h3>
            <a href="new-order.html">order now</a>
            <ul>
                <li><strong>200 capsules/month</strong></li>
                <li>Fresh, tastefull coffee</li>
                <li>Drop-off point delivery</li>
                <li>inc. tax & shipping </li>
            </ul>
        </div>
        <div class="fourcol">
            <h3 style="font-size: 28px; padding: 22px 20px 15px 20px;"><span></span>on request</h3>
            <a href="new-order.html">order now</a>
            <ul>
                <li><strong>400 capsules/month</strong></li>
                <li>Perfect for small businesses</li>
                <li>Elastic subscription</li>
                <li>inc. coffee consulting </li>
            </ul>
        </div>
    </div>
</div>



<div id="instructionsContainer" class="wrapper clearfix">
    <!-- INSTRUCTIONS BLOCK -->
    <article id="instructions">
        <div class="articleContent">
            <header>
                <h1>How does it work?</h1>
                <p>You simply decide how many capsules you want in your mailbox each month and what flavour/strenght you fancy. Once you’re done with that we’ll need your address and credit card number, and we will solve the rest.</p>
            </header>
            <section>
                <h2>1. Pick your subscription</h2>
                <p>Pick how many capsules you want and how often you want them. You can also gift a subscription to a family member or friend.</p>
            </section>
            <section>
                <h2>2. Customize & checkout</h2>
                <p>Pick the coffee flavour & strenght and how long you want the subscription to last we’ll take care of the rest. </p>
            </section>
            <h2>3. Mailbox delivery</h2>
            <p>Now you can relax and you don’t have to run to the store every other day to buy new coffee capsules for your machine. </p>
            <section>
        </div>
        <div class="articleImage">

        </div>

    </article>
</div> <!-- #instructions-container -->



<div id="lifestyleContainer" class="clearfix">

    <div class="fiveColFirst">
        <img src="img/inspiration/1.jpg" alt="" title="" />
        <img src="img/inspiration/2.jpg" alt="" title="" />
        <img src="img/inspiration/3.jpg" alt="" title="" />
    </div>
    <div class="fiveCol">
        <article>
            <h3>Your favorite coffee every day</h3>
            <p>Regardless of where you live and
                what city, country or spot you find
                yourself at, we provide the best coffee
                capsule subscription service on the
                planet. Get your favorite coffee perfect
                clean and comfortable every day.
                <br /><br />
                <a href="new-order.html">Customize your order</a></p>

        </article>
        <img src="img/inspiration/4.jpg" alt="" title="" />
        <img src="img/inspiration/5.jpg" alt="" title="" />

    </div>
    <div class="fiveCol">
        <img src="img/inspiration/6.jpg" alt="" title="" />
        <img src="img/inspiration/7.jpg" alt="" title="" />
        <img src="img/inspiration/8.jpg" alt="" title="" />
    </div>
    <div class="fiveCol">
        <img src="img/inspiration/9.jpg" alt="" title="" />
        <img src="img/inspiration/10.jpg" alt="" title="" />
        <img src="img/inspiration/11.jpg" alt="" title="" />
    </div>
    <div class="fiveColLast">
        <img src="img/inspiration/12.jpg" alt="" title="" />
        <img src="img/inspiration/13.jpg" alt="" title="" />
        <img src="img/inspiration/14.jpg" alt="" title="" />

    </div>

</div>

@stop
