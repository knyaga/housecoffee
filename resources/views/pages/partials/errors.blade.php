@if($errors->any())
    <strong>There were some problems with your input.</strong> <br><br>
   <ul class="errors">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
   </ul>
@endif
