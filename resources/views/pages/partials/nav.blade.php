<nav id="navLeft">
    <ul class="menu">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('coffee') }}" >The coffee</a></li>
        <li><a href="/subscription">Subscription</a></li>
    </ul>
</nav>
<h1 class="title">House Coffee Club</h1>
<nav id="navRight">
    <ul class="menu">
        <li>
            @if( Auth::check() )
              <a href="{{ url('/auth/logout') }}">Logout</a>
            @endif
        </li>
        <li><a href="{{ route('account') }}">My Account</a></li>
        <li><a href="{{ route('faq') }}">Faq</a></li>
        <li><a href="{{ route('about-us') }}">About us</a></li>
    </ul>
</nav>
