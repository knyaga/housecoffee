<header class="wrapper clearfix" style="padding-top: 46;">

    <h1 class="title">House Coffee Club</h1>
    <nav id="navRight">
        <ul>
            <li><a href="{{ url('auth/logout') }}">Logout</a></li>
            <li><a href="{{ route('account') }}" class="selected">My account</a></li>
        </ul>
    </nav>
</header>

<div id="subHeroContainer" class="wrapper clearfix heroFont" style="margin-top:0;">
    <h2 class="animated fadeInRight">Welcome {{ $user->firstName }} , this is your control panel</h2>
</div>
