<div id="subAccountMenu" class="wrapper clearfix">
    <ul>
        <li><a href="{{ route('account') }}" class="subActive">My Subscription Plan</a></li>
        <li>{!! link_to_action('AccountsController@getShipping','Shipping Details',$user->id) !!}</li>
        <li><a href="{{ route('card_details') }}">Update Credit Card</a></li>
    </ul>
</div>
