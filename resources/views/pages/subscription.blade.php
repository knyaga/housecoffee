@extends('master')

@section('content')

<div id="priceTiers">

    <div id="announcemnet" class="">

        <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>

    </div>

    <header class="wrapper clearfix" style="padding-top: 46;">

        @include('pages.partials.nav')

    </header>

    <div id="subHeroContainer" class="wrapper clearfix heroFont" style="margin-top:0;">
        <h2 class="animated fadeInRight">Tastefull, clean and comfortable delivery in your mailbox</h2>
    </div>
    <div id="priceContainer" class="wrapper">
        <div class="fourcol">
            <h3><span>mini</span>15€</h3>
            <a href="#">order now</a>
            <ul>
                <li><strong>50 capsules/month</strong></li>
                <li>Fresh, tastefull coffee</li>
                <li>Mailbox delivery</li>
                <li>inc. tax & shipping </li>
            </ul>
        </div>
        <div class="fourcol popular">
            <h3><span>midi</span>29€</h3>
            <a href="#">order now</a>
            <ul>
                <li><strong>100 capsules/month</strong></li>
                <li>Fresh, tastefull coffee</li>
                <li>Mailbox delivery</li>
                <li>inc. tax & shipping </li>
            </ul>
        </div>
        <div class="fourcol">
            <h3><span>maxi</span>59€</h3>
            <a href="#">order now</a>
            <ul>
                <li><strong>200 capsules/month</strong></li>
                <li>Fresh, tastefull coffee</li>
                <li>Drop-off point delivery</li>
                <li>inc. tax & shipping </li>
            </ul>
        </div>
        <div class="fourcol">
            <h3 style="font-size: 28px; padding: 22px 20px 15px 20px;"><span></span>on request</h3>
            <a href="#">order now</a>
            <ul>
                <li><strong>400 capsules/month</strong></li>
                <li>Perfect for small businesses</li>
                <li>Elastic subscription</li>
                <li>inc. coffee consulting </li>
            </ul>
        </div>
    </div>
</div>

<div id="orderFormContainer" class="wrapper clearfix">

    <div id="HccForm" class="animated fadeInLeft">

        <!-- DISPLAYS VALIADATION ERRORS -->
        @include('pages/partials/errors')

        {!! Form::open(['action'=>'SubscriptionController@postForm','id'=>'subscription-form']) !!}
            <!-- MAKE PACKAGE SELECTION -->
            <h2>Select package</h2>
            <select id="order_quantity" name="plan" value="{{ old('plan') }}">
                <option value="mini" data-price="15€">Mini 50 capsules/monthly - 15€</option>
                <option selected="selected" value="midi" data-price="29€">Midi 100 capsules/monthly - 29€</option>
                <option value="maxi" data-price="59€">Maxi 200 capsules/monthly - 59€</option>
                <option value="onRequest" data-price="119€">On request - minimum 400 capsules/monthly - from 119€</option>
            </select>

            <h2>Email & Password</h2>

            <input class="email required"     name="email" placeholder="Email" type="email" value="{{ old('email') }}">
            <input class="password required"  name="password" placeholder="Password" type="password">
            <input class="password required"  name="password_confirmation" placeholder="Confirm password" type="password">

            <!-- ENTER SHIPPING ADDRESS -->
            <h2>Enter your shipping address</h2>
            <input class="firstname required" id="first_name" name="firstName" placeholder="First name" type="text" value="{{ old('firstName') }}">
            <input class="lastname required" id="lastname_name" name="lastName" placeholder="Last name" type="text" value="{{ old('lastName') }}">
            <input class="phone optional" id="user_phone" name="mobile" placeholder="Mobile number" type="text" value="{{ old('mobile') }}">
            <input class="address required" id="address1" name="address1" placeholder="Address" type="text" value="{{ old('address1') }}">
            <input class="address required" id="address2" name="address2" placeholder="Address 2" type="text" value="{{ old('address2') }}">
            <input class="zip required" id="zip" name="postalCode" placeholder="Postal Code" type="text" value="{{ old('postalCode') }}">
            <input class="required" id="city" name="city" placeholder="City, Country" type="text" value="{{ old('city') }}">

            <!-- Header -->
            <h2 class="left">Payment Information</h2>

            <!-- DISPLAYS STRIPE ERRORS -->
            <div class="stripe-errors hide"></div>

            <!-- CREDIT CARD INFORMATION -->
            <input class="creditcard required"  data-stripe ="number" name="cardNumber" placeholder="Creditcard number" type="creditcard" value="{{ old('cardNumber') }}">
            <input class="month required payThird" data-stripe ="exp-month" name="expiryMonth" placeholder="month (dd)" type="text" value="{{ old('expiryMonth') }}" >
            <input class="year required payThird" data-stripe="exp-year" name="expiryYear" placeholder="year (yy)" type="text" value="{{ old('expiryYear') }}">
            <input class="cvc required payThird" data-stripe="cvc" name="cvc" placeholder="cvc/cvv" type="text" value="{{ old('cvc') }}" style="margin-right: 0;">

            <!-- ORDER TOTAL WILL BE SHOWN HERE -->
            <h2 class="left">Monthly Total:</h2>
            <h2 class="right" style="font-weight: normal" id="planTotal"></h2>

            <!-- DISCLAIMER AND INFORMATION ABOUT THE SALES -->
            <p class="order-form-terms-disclaimer">
                Payment is handled securly by <a href="http://stripe.com" target="_new">stripe.com</a>, you can use your standard credit card to pay.<br>
                By placing an order your agreeing on our <a href="/terms">Sales Conditions, Terms</a> &amp; <a href="/retur">Return policy</a>
            </p>

            <!-- SEND FORM DATA TO PAYPAL FOR RECURRING PAYMENTS SETUP -->
            <input class="buttonPrimary" name="button" type="submit" id="stripeCommandButton" value="Confirm Subscription & Create Account">

        {!! Form::close() !!}

    </div>

</div>



@stop

@section('scripts')

    <!-- MAKE SURE JAVASCRIPT IS ENABLED -->
    <noscript><p>JavaScript is required for the registration form.</p></noscript>

    <script src="https://js.stripe.com/v2/"></script>
    <script src="/js/stripe.js"></script>

@stop

