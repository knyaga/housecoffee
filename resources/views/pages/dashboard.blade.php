@extends('master')

@section('content')

        <div id="priceTiers" style="min-height: 300px;">

            <div id="announcemnet" class="">

                <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>

            </div>

            <!-- INCLUDES MENU LINKS -->
            @include('pages.partials.dashboard-nav')

        </div>

        <!-- INCLUDES SUB-ACCOUNT MENU -->
        @include('pages.partials.subAccountMenu')


        <div id="orderFormContainer" class="wrapper clearfix">

            <div id="HccForm" class="animated fadeInLeft">
            {!! Form::open(['action'=>'AccountsController@swapMonthlySubscription']) !!}
                <!-- MAKE PACKAGE SELECTION -->
                <h2>Edit Your Active Monthly Plan</h2>
                <small>Click to change monthly plan</small><br>

                @if(Session::has('plan'))
                    <p>You are now subscribe to {{ Session::get('plan') }}</p>
                @endif

                <select id="order_quantity" name="plan">
                    <option value="mini">Mini 50 capsules/monthly - 15€</option>
                    <option selected="selected" value="midi">Midi 100 capsules/monthly - 29€</option>
                    <option value="maxi">Maxi 200 capsules/monthly - 59€</option>
                    <option value="onRequest">On request - minimum 400 capsules/monthly - from 119€</option>
                </select>
                <input class="buttonPrimary" name="button" type="submit" value="Change subscription" />
            {!! Form::close() !!}

            </div>

        </div>


@stop

@section('scripts')

    <!-- MAKE SURE JAVASCRIPT IS ENABLED -->
    <noscript><p>JavaScript is required for the registration form.</p></noscript>

    <script src="https://js.stripe.com/v2/"></script>
    <script src="/js/stripe.js"></script>

@stop
