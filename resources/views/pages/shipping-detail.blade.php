@extends('master')

@section('content')

        <div id="priceTiers" style="min-height: 300px;">

            <div id="announcemnet" class="">

            <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>

            </div>
            <!-- INCLUDES MENU LINKS -->
            @include('pages.partials.dashboard-nav')

        </div>

        <!-- INCLUDES SUB-ACCOUNT MENU -->

        @include('pages.partials.subAccountMenu')


        <div id="orderFormContainer" class="wrapper clearfix">

            <div id="HccForm" class="animated fadeInLeft">

            {!! Form::model($user,['method'=>'PATCH','action'=>['AccountsController@updateShipping',$user->id] ])!!}
                <h2>Edit or Change your shipping address</h2>
                <h2>
                    @include('pages.partials.errors')
                </h2>
                <input class="firstname required" id="first_name" name="firstName" placeholder="First name" type="text" value="{{ $user->firstName }}">
                <input class="lastname required" id="lastname_name" name="lastName" placeholder="Last name" type="text"  value="{{ $user->lastName }}">
                <input class="phone optional" id="user_phone" name="mobile" placeholder="Mobile number" type="tel"  value="{{ $user->mobile }}">
                <input class="address required" id="address1" name="address1" placeholder="Address" type="text"  value="{{ $user->address1 }}">
                <input class="address required" id="address2" name="address2" placeholder="Address 2" type="text"  value="{{ $user->address2 }}">
                <input class="zip required" id="zip" name="postalCode" placeholder="Postal Code" type="text"  value="{{ $user->postalCode }}">
                <input class="required" id="city" name="city" placeholder="City, Country" type="text"  value="{{ $user->city }}">
                <input class="buttonPrimary" name="button" type="submit" value="Update Shipping Adress" id="checkoutButton"/>
            {!! Form::close() !!}

            </div>

        </div>

@stop
