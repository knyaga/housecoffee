@extends('master')

@section('content')

<div class="header-container" style="background-image: url(img/jpg/sub-hero-2.jpg);">

    <div id="announcemnet" class="">

        <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location. <a href="{{ route('subscription') }}">Start subscription</a></p>

    </div>

    <header class="wrapper clearfix">

        @include('pages.partials.nav')

        <div id="heroContainer" class="clearfix heroFont animated fadeInDown" style="text-align: center;">
            <h1>All you need to know about the coffee</h1>
            <h2>Freshly grounded, packaged and delivered to your location.</h2>

            <a href="/subscription" class="button">Start Subscription</a>
        </div>
    </header>
</div>


<div id="instructionsContainer" class="wrapper clearfix">
    <!-- INSTRUCTIONS BLOCK -->
    <article id="instructions">
        <div class="articleContent">
            <header>
                <h1>The perfect coffee every time.</h1>
                <p>You simply decide how many capsules you want in your mailbox each month and what flavour/strenght you fancy. Once you’re done with that we’ll need your address and credit card number, and we will solve the rest.</p>
            </header>
            <section>
                <h2>1. Pick your subscription</h2>
                <p>Pick how many capsules you want and how often you want them. You can also gift a subscription to a family member or friend.</p>
            </section>
            <section>
                <h2>2. Customize & checkout</h2>
                <p>Pick the coffee flavour & strenght and how long you want the subscription to last we’ll take care of the rest. </p>
            </section>
            <h2>3. Mailbox delivery</h2>
            <p>Now you can relax and you don’t have to run to the store every other day to buy new coffee capsules for your machine. </p>
            <section>
        </div>
        <div class="articleImage">

        </div>

    </article>
</div> <!-- #instructions-container -->

@stop
