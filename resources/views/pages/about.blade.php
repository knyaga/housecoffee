@extends('master')

@section('content')
<div class="header-container" style="background-image: url(img/jpg/sub-hero-3.jpg); background-position: center bottom;">

    <div id="announcemnet" class="">

        <p><img src="{{ asset('img/flag/spain.gif') }}" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location. <a href="{{ route('subscription') }}">Start subscription</a></p>

    </div>

    <header class="wrapper clearfix">

        @include('pages.partials.nav')

        <div id="heroContainer" class="clearfix heroFont animated fadeInDown" style="text-align: center;">
            <h1>We love coffee, but we also love simplicity</h1>
            <h2>Capsule coffee is fast, simple, and very easy to use.</h2>

            <a href="/subscription" class="button">Start Subscription</a>
        </div>
    </header>
</div>


<div id="instructionsContainer" class="wrapper clearfix">
    <!-- INSTRUCTIONS BLOCK -->
    <article id="instructions">
        <div class="articleContent faq" style="margin: 0 auto; float: none; width: 70%;">
            <header>
                <h1>Our story</h1>


                <p>House Coffee Club established in  the early 2015 in Barcelona. That was the time when we, as great coffee lovers, felt that the routines at home according to the coffee consumption had to improve. Like many others the morning routines can get a bit hectic, hence to a various amount of obstacles such as family, too little sleep, too much to do and too little time. </p>

                <p>Despite all the hassle during the mornings one thing was for sure, we still need our beloved coffee. Thanks to the timesaving and easy Nespresso machine that are a common objects in many homes these days we can now save a lot of time during stressful mornings or afternoons. BUT, we all had discovered a huge problem that, once again, made our precious cup of coffee non-exictent. The absence of coffee capsules began to be a standing subject on our morning chats. </p>

                <p>There are some things that you always want in your home. Water, toilet paper, electricity and most important - COFFEE. That was when we came up with the brilliant idea to make this happen, not only for us but to all coffee lovers out there. Why not make coffee capsules delivered right to your door every month? Just choose a subscription, make the payment and your good to go all year around. </p>


            </header>
        </div>

    </article>
</div> <!-- #instructions-container -->

@stop
