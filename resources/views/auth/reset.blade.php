@extends('master')

@section('content')

    <div id="priceTiers">

        <div id="announcemnet" class="">

            <p><img src="/img/flag/spain.gif" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>

        </div>

        <header class="wrapper clearfix" style="padding-top: 46;">

            <!-- RENDERS THE MAIN NAVIGATION MENU -->
            @include('pages.partials.nav')

        </header>

        <div id="subHeroContainer" class="wrapper clearfix heroFont" style="margin-top:0;">
            <h2 class="animated fadeInRight">Reset password</h2>
        </div>

        <div id="orderFormContainer" class="wrapper clearfix">

            <div id="HccForm" class="animated fadeInLeft">

                <!-- DISPLAYS VALIDATION ERRORS -->

                @include('pages/partials/errors')

                <form method="POST" action="{{ url('/password/reset') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input class="email required"  name="email" placeholder="Email" type="text" value="{{ old('email') }}" >
                    <input class="password required" name="password" placeholder="Password" type="password" required="">
                    <input class="password required"  name="password_confirmation" placeholder="Confirm password" type="password" >
                    <input class="buttonLogin" name="button" type="submit" value="Reset password" />
                </form>

            </div>

        </div>

    </div>

@stop

