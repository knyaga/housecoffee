@extends('master')

@section('content')

<div id="priceTiers">

    <div id="announcemnet" class="">

        <p><img src="/img/flag/spain.gif" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>

    </div>

    <header class="wrapper clearfix" style="padding-top: 46;">

        <!-- RENDERS THE MAIN NAVIGATION MENU -->
        @include('pages.partials.nav')

    </header>

    <div id="subHeroContainer" class="wrapper clearfix heroFont" style="margin-top:0;">
        <h2 class="animated fadeInRight">Login To Access Your Account</h2>
    </div>

    <div id="orderFormContainer" class="wrapper clearfix">

        <div id="HccForm" class="animated fadeInLeft">

            <!-- DISPLAYS VALIDATION ERRORS -->
            @include('pages/partials/errors')

            {!! Form::open(['url'=>'/auth/login']) !!}

                <!-- LOGIN FORM -->
                <input class="email required" id="user_email" name="email" placeholder="Email" type="text" value="{{ old('email') }}" required="">
                <input class="password required" id="password" name="password" placeholder="Password" type="password" required="">


                <!-- LOGIN BUTTON -->
                <input class="buttonLogin" name="button" type="submit" value="Login" id="checkoutButton"/>

                <!-- RESET PASSWORD LINK -->
                <a href="{{ url('password/email') }}" style="color:#fff; text-decoration: none; text-align:center; width:100%; display:block; margin-top: 5px; ">Forgot Your Password?</a>

            {!! Form::close() !!}

        </div>

    </div>

</div>

@stop

