@extends('master')

@section('content')

    <div id="priceTiers">

        <div id="announcemnet" class="">

            <p><img src="/img/flag/spain.gif" width="16" height="11" class="flag"> Hola! We're happy to provide free shipping to your current location.</p>

        </div>

        <header class="wrapper clearfix" style="padding-top: 46;">

            <!-- RENDERS THE MAIN NAVIGATION MENU -->
            @include('pages.partials.nav')

        </header>

        <div id="subHeroContainer" class="wrapper clearfix heroFont" style="margin-top:0;">
            <h2 class="animated fadeInRight">Reset password</h2>
        </div>

        <div id="orderFormContainer" class="wrapper clearfix">

            <div id="HccForm" class="animated fadeInLeft">

                <!-- DISPLAYS VALIDATION ERRORS -->

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @include('pages/partials/errors')

                <form  method="POST" action="{{ url('/password/email') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input class="email required" id="user_email" name="email" placeholder="Email" type="text" value="{{ old('email') }}" required="">
                    <input class="buttonLogin" name="button" type="submit" value="Send Password Reset Link" />
                    <a href="{{ url('/auth/login') }}" style="color:#fff; text-decoration: none; text-align:center; width:100%; display:block; margin-top: 5px; ">Login</a>
               </form>

            </div>

        </div>

    </div>

@stop


