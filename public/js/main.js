
$(document).ready(function(){

    //Get selected plan price

    var $selectTag    = $("#order_quantity");
    var $selectedPlanPrice = $selectTag.find(":selected").data('price');

    //Initial selected plan
    $("#planTotal").text($selectedPlanPrice);

    $selectTag.on('change', function(){

        var selectedPlanPrice = $selectTag.find(":selected").data('price');
        $("#planTotal").text(selectedPlanPrice);

    });


});
