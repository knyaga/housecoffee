/**
 * Created by otis on 26/03/15.
 */

$(document).ready(function(){

    //Stripe functionality
    Stripe.setPublishableKey('pk_test_sWd1u9xCmfjmJidi5lQJv7E2');

    $("#stripeCommandButton").on('click', function(){

        var $form = $("#subscription-form");
        var stripeCommandButton = $("#stripeCommandButton");
        var subscribeButtonText = stripeCommandButton.val();

        stripeCommandButton.attr('disabled','disabled').val('PROCESSING DETAILS YOUR DETAILS ...');

        Stripe.card.createToken($form, function(status, response){

            var token; // stripe token

            if(response.error)
            {
                var errorMessage = response.error.message;
                $(".stripe-errors").text(errorMessage).show();

                console.log(errorMessage);

                stripeCommandButton.removeAttr('disabled').val(subscribeButtonText);

            }else {
                token = response.id;
                $form.append($('<input type="hidden" name="stripeToken"/>').val(token));
                $form.submit();
            }
        });

    });

});
