<?php namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        /**
         * Fetches the auth id and pass to partial
         * use for edit profile
         */

        view()->composer('pages.partials.subAccountMenu', function($view){

            $user = Auth::user();

            $view->with('user',$user);
        });

        view()->composer('pages.partials.dashboard-nav', function($view){

            $user = Auth::user();

            $view->with('user',$user);
        });
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
