<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract,BillableContract {

    use Authenticatable, CanResetPassword;

    use Billable;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstName', 'lastName', 'email', 'password','mobile','address1','address2','postalCode','city',''];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /** Checks if the authenticated user is admin
     * @return bool
     */
    public function isAdmin()
    {
        //Logic comes here
        return false;
    }

    public function addCurrencySymbol($amount)
    {
        return '£'.$amount;
    }

    public function getBillableName()
    {
        return $this->firstName."  ".$this->lastName;
    }



}
