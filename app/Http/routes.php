<?php

/**
 * Available menu pages routes
 */



Route:get('/test',function(){

	/**

		House Coffee Club by Copygram AB

		Storgatan 31

		311 31 - Falkenberg
		Phone: 0046(0)346-92302

		Organisationsnummer: 556866-6654

		VAT-number: SE5568666665401

    */

	$user = Auth::user();

	echo '<pre>';
 	var_dump($user->invoices());
	echo '</pre>';

	die();

	$invoices = $user->subscription()->invoices();

	foreach($invoices as $invoice ) {

		return $receipt = $user->downloadInvoice($invoice->id,[
			'vendor'  => 'House Coffee Club by Copygram AB',
			'product' => 'coffee',
			'street'  => 'Storgatan 31 ,311 31 - Falkenberg',
			'phone'   =>  '0046(0)346-92302',
			'Organisationsnummer' => '556866-6654',
			'vat' =>'SE5568666665401'
		]);

	}





});

Route::get('/',['as'=>'home', 'uses'=>'PagesController@home']);
Route::get('/home',['as'=>'home', 'uses'=>'PagesController@home']);
Route::get('/faq',['as'=>'faq','uses'=>'PagesController@faq']);
Route::get('/the-coffee',['as'=>'coffee','uses'=>'PagesController@theCoffee']);
Route::get('/about-us',['as'=>'about-us','uses'=>'PagesController@about']);


/** Subscription route: frontend form*/

Route::get('/subscription', ['as'=>'subscription','uses'=>'SubscriptionController@getForm']);
Route::get('/thank-you', ['as'=>'thank-you','uses'=>'SubscriptionController@getThankYou']);
Route::post('/subscription','SubscriptionController@postForm');


/**
 * User account operation routes
 */

Route::get('/my-account',          ['as'=>'account','uses'=>'AccountsController@getDashboard']);
Route::post('/subscription/cancel',['before'=>'csrf','as'=>'cancel-subscription','uses'=>'AccountsController@cancelSubscription']);
Route::post('/subscription/resume',['before'=>'csrf','as'=>'resume-subscription','uses'=>'AccountsController@resumeCancelledSubscription']);
Route::post('/subscription/change',['before'=>'csrf','as'=>'change-subscription','uses'=>'AccountsController@swapMonthlySubscription']);
Route::get('/shipping-details/{id}/edit',['as'=>'edit_shipping','uses'=>'AccountsController@getShipping']);
Route::patch('/shipping-details/{id}','AccountsController@updateShipping');
Route::get('/card-details',['as'=>'card_details','uses'=>'AccountsController@getCard']);
Route::post('/card-details/update','AccountsController@updateCard');


/**
 * Authentication routes
 */

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
