<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AccountDetailRequest;
use Auth;
use App\User;
use Route;
use Request;

class AccountsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function getDashboard()
    {
        $user = Auth::user();

        return view('pages.dashboard',compact('user'));
    }

    public function getShipping($id)
    {
        $user = User::findOrFail($id);

        return view('pages.shipping-detail',compact('user'));
    }

    public function updateShipping($id,AccountDetailRequest $request)
    {
        $user = User::findOrFail($id);

        $user->update($request->all());

        return redirect()->route('edit_shipping',$id);
    }



    public function getCard()
    {
        $user = Auth::user();

       return view('pages.card-detail',compact('user'));
    }


    public function swapMonthlySubscription()
    {
        $user = Auth::user();
        
        $selectedPlan = Request::input('plan');
        

        if( Auth::check() && $user->subscribed() )
        {
            $user->subscription($selectedPlan)->swap();

            return redirect()->route('account')->with('plan',$user->stripe_plan);
        }

    }

    public function cancelSubscription()
    {
        $user = Auth::user();

        if( Auth::check() && $user->subscribed() )
        {
            $user->subscription()->cancel();

            return redirect()->route('card_details')->with('message','We are sad to see you go.');
        }

    }

    public function resumeCancelledSubscription()
    {
        $user = Auth::user();

        if(Auth::check() && $user->cancelled())
        {
            $user->subscription($user->stripe_plan)->resume();

            return redirect()->route('card_details')->with('message','Welcome back!!');
        }

    }

    public function updateCard()
    {

        $token = Request::input('stripeToken');

        if( Auth::check() && Auth::user()->subscribed() )
        {
            Auth::user()->updateCard($token);

            return redirect()->route('card_details')->with('card_message','You have successfully changed you card details');
        }
    }

    public function getInvoiceDetails()
    {
        $user = Auth::user();

        dd($user->getStripePlan());

    }

}
