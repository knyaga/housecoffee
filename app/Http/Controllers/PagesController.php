<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;



class PagesController extends Controller {

    public function home()
    {
        return view('pages.home')->withSelected('selected');
    }

    public function theCoffee()
    {
        return view('pages.the-coffee');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function faq()
    {
        return view('pages.faq');
    }



}
