<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SubscriptionRequest;
use Illuminate\Support\Facades\Mail as Mail;
use App\User;
use Exception;
use Request;
use DB;
use Hash;

class SubscriptionController extends Controller {

    /**
     * @var First name of new member
     */
    protected $firstName;

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Renders subscription form
     */

	public function getForm()
    {
        return view('pages.subscription')->with('selected','selected');
    }

    /**
     * Saves client detail and subscribe to plan
     * @param SubscriptionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function postForm(SubscriptionRequest $request)
    {


        DB::beginTransaction();

        $creditCardToken = Request::input('stripeToken');
        $plan = Request::input('plan');

        try
        {
            $user = User::create([
                'firstName'  => $request->input('firstName'),
                'lastName'   => $request->input('lastName'),
                'password'   => Hash::make($request->input('password')),
                'email'      => $request->input('email'),
                'mobile'     => $request->input('mobile'),
                'address1'   => $request->input('address1'),
                'address2'   => $request->input('address2'),
                'postalCode' => $request->input('postalCode'),
                'city'       => $request->input('city')
            ]);
        }
        catch (Exception $e)
        {
            DB::rollback();
            throw $e;
        }

        try
        {
            $user->subscription($plan)->create($creditCardToken, [
                'email' => $user->email,
                'metadata' => [
                    'First Name'    => $user->firstName,
                    'Last Name'     => $user->lastName,
                    'Mobile number' => $user->mobile,
                    'Address'       => $user->address1,
                    'City'          => $user->city,
                    'Zip'           => $user->postalCode

                ]
            ]);

        }
        catch(Exception $e)
        {
            DB::rollback();
            throw $e;
        }


		/**
			Send email receipt
		*/

		/*
        Mail::send('pages.thank-you',(array)$user, function($message){

            $message->to('kennedy.otis@gmail.com', 'kennedy')->subject('Welcome!');
        }); */



        DB::commit();
        return view('pages.thank-you')->with('user',$user);

    }

    /** Renders the thank you page
     * @return \Illuminate\View\View
     */

    public function getThankYou()
    {
        return view('pages.thank-you');
    }


}
