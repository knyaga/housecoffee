<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubscriptionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'cardNumber'   => 'required',
            'expiryMonth'  => 'required',
            'expiryYear'   => 'required',
            'cvc'          =>'required',
            'firstName'    => 'required',
            'lastName'     => 'required',
            'email'        => 'required|email|unique:users',
            'address1'     => 'required',
            'password'     => 'required|confirmed|min:8',
		];
	}

}
